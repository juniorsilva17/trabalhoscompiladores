#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *token;
int pos = 0;

int exp_int(void);
int term_int(void);
int fator_int(void);

float exp_float(void);
float term_float(void);
float factor_float(void);

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

float potencia_float(float n, float x){
	float res = 1.0;
	if(x == 0){
		return 1;
	}
	if (x < 0)
		error();
	for (int i = x; i >= 1; i--){
		res *= n;
	}
	return res;
}

int potencia_int(int n, int x){
	int res = 1;
	if(x == 0){
		return 1;
	}
	if (x < 0)
		error();

	for (int i = x; i >= 1; i--){
		res *= n;
	}
	return res;
}

void match(char expectedToken){
	if(token[pos] == expectedToken)
		pos++;
	else{
		error();
	}
}

float exp_float(void){
	float temp = term_float();
	while ((token[pos] == '+') || (token[pos] =='-'))
		switch(token[pos]){
			case '+' :
			match('+');
			temp+=term_float();
			break;
			case '-':
			match('-');
			temp-=term_float();
			break;
		}
		return temp;
	}
	int exp_int(void){
		int temp = term_int();
		while ((token[pos] == '+') || (token[pos] =='-'))
			switch(token[pos]){
				case '+' :
				match('+');
				temp += term_int();
				break;
				case '-':
				match('-');
				temp -=term_int();
				break;
			}
			return temp;
		}

		float exfactor_float(void){
			float temp = factor_float();
			switch(token[pos]){
				case '^':
				match('^');
				temp = potencia_float(temp, factor_float());
				break;
			}
			return temp;
		}

		int exfactor_int(void){
			int temp = factor_int();
			switch(token[pos]){
				case '^':
				match('^');
				temp = potencia_int(temp, factor_int());
				break;
			}
			return temp;

		}

		float term_float(void){
			float temp = exfactor_float();
			while((token[pos] =='*') || (token[pos] == '/')|| (token[pos] == '%')){
				switch(token[pos]){
					case '*':
					match('*');
					temp *= exfactor_float();
					break;
					case '/':
					match('/');
					temp = temp/exfactor_float();
					break;
					case '%':
					match('%');
					temp = (float) ((int)temp % (int) exfactor_float());
					break;

				}
			}
			return temp;
		}

		int term_int(void){
			int temp = exfactor_int();
			while((token[pos] =='*') || (token[pos] == '/')|| (token[pos] == '%')){
				switch(token[pos]){
					case '*':
					match('*');
					temp *= exfactor_int();
					break;
					case '/':
					match('/');
					temp = temp/exfactor_int();
					break;
					case '%':
					match('%');
					temp = temp % exfactor_int();
					break;

				}
			}
			return temp;
		}

		float factor_float(void){
			float temp;
			if(token == '('){
				match('(');
				temp = exp_float();
				match(')');
			}
			else if(isdigit(token[pos]) || token[pos] == '-'){
				int flag3 = 0;
				if (token[pos] == '-'){
					pos++;
					flag3 = 1;
				}

				temp = (float) (token[pos] - 48);
				pos++;

				float flag = 1;
				int flag2 = 10;

				while(token[pos] == '.' || isdigit(token[pos])){
					if (token[pos] == '.'){
						flag = 0.1;
						flag2 = 1;
					}
					if (token[pos] != '.'){
						temp = temp*flag2 + (((float) (token[pos] - 48))*flag);
					}
					pos++;
				}
				if (flag3 == 1){
					temp *= -1;
				}
			}
			else{
				error();
			}
			return temp;
		}

		int factor_int(void){
	printf("a");

	int temp;
	if(token[pos] == '('){
		match('(');
		temp = exp_int();
		match(')');
	}
	else if(isdigit(token[pos]) || token[pos] == '-'){

		int flag = 0;
		if (token[pos] == '-'){
			pos++;
			flag = 1;
		}

		temp = (int) (token[pos] - 48);
		pos++;

		while(isdigit(token[pos])){
			temp = temp*10 + ((int) (token[pos] - 48));
			pos++;
		}

		if (flag == 1){
			temp *= -1;
		}

	}
	else{
		error();
	}
	return temp;
}
		int main(){
			float result1 = 0.0;
			int result2 = 0;

			size_t bufsize = 32;
			size_t characters;

			int flag = 0;

			token = (char *)malloc(bufsize * sizeof(char));
			if( token == NULL)
			{
				perror("Unable to allocate buffer");
				exit(1);
			}
			characters = getline(&token,&bufsize,stdin);

			for(int i = 0; i < characters; i++){
				if (token[i] == '.'){
					flag = 1;
				}
			}

			if(flag == 0){
				result2 = exp_int();
				printf("Result = %d\n", result2);
			}else{
				result1 = exp_float();
				printf("Result = %f\n", result1);
			}

			return 0;
		}
