%{/*	Programa lex para converter em caixa alta os comentarios da linguagem C.		
	lex version 2.6.0
	Linux junior-Aspire-E5-574G 4.13.0-36-generic #40~16.04.1-Ubuntu SMP Fri Feb 16 23:25:58 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
	*/
	
#include <stdio.h>
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif
#ifndef yywrap
static int yywrap (void) {return 1;}
#endif
%}

%% 

"/*" { 	char c;
	int done = FALSE;
	ECHO;
	do{	
		while((c = input())!='*')
			putchar(toupper(c));
			putchar(toupper(c));
		while((c = input()) == '*')
			putchar(toupper(c));
			putchar(toupper(c));
		if(c == '/') done = TRUE;
	}while(!done);
	
}
%%
void main(void){
yylex();
}



