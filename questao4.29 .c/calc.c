/* Calculadora para aritmética de inteiros simples segundo a EBNF:
 * <exp> -> <termo> { <soma> <termo> }
 * <soma> -> + | -
 * <termo> -> <fator> { <mult> <fator> }
 * <mult> -> *
 * <fator> -> ( <exp> ) | Número
 */

#include <stdio.h>
#include <stdlib.h>
char token; //variável de marca global

//protótipos de funções para ativações recursivas
int exp(void);
int term(void);
int factor(void);

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

int potencia(int n, int x){
	int res = 1;
	if (x < 0)
		error();
	if(x == 0){
		return 1;
	}
	for (int i=x ; i>=1 ; i--){
		res *= n;
	}
	return res;
}

void match(char expectedToken){
	if(token == expectedToken)
		token = getchar();
	else{
		error();
	}
}

int exp(void){
	int temp = term();
	while ((token == '+') || (token=='-'))
		switch(token){
			case '+' :
				match('+');
				temp+=term();
				break;
			case '-':
				match('-');
				temp-=term();
				break;
		}
		return temp;
}

int exfactor(void){
	int temp = factor();
	switch(token){
		case '^':
			match('^');
			temp = potencia(temp, factor());
			break;
	}
	return temp;

}

int term(void){
	int temp = exfactor();
	while((token=='/') || (token == '%')|| (token == '*')){
		switch(token){
			case '/':
				match('/');
				temp = temp/exfactor();
				break;
			case '%':
				match('%');
				temp = temp % exfactor();
				break;
			case '*':
				match('*');
				temp *= exfactor();
				break;
			

		}
	}
	return temp;
}

int factor(void){
	int temp;
	if(token == '('){
		match('(');
		temp = exp();
		match(')');
	}
	else if(isdigit(token)){
		ungetc(token, stdin);
		scanf("%d", &temp);
		token = getchar();
	}
	else{
		error();
	}
	return temp;
}

int main(){
	int result;
	token = getchar(); //carga de marca com primeiro caractere para verificação à frente
	result = exp();

	printf("%c\n", token);

	if(token == '\n') //teste final de linha
		printf("Result = %d\n", result);
	else{
		printf("X\n");
		error();
	}
	return 0;
}
