%{/*	Programa lex para converter em caixa baixa caracteres fora dos comentarios na linguagem C.
	lex version 2.6.0
	Linux junior-Aspire-E5-574G 4.13.0-36-generic #40~16.04.1-Ubuntu SMP Fri Feb 16 23:25:58 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
*/


#include <stdio.h>
#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif
int inComment = FALSE;
#ifndef yywrap
static int yywrap (void) {return 1;}
#endif
%}

%%

[A-Z] { 
	if(!inComment){
		putchar(tolower(yytext[0]));
	}
	else{
		putchar(yytext[0]);
	}
}

"/*" {
	ECHO;
	inComment = TRUE;
     	}

"*/" {
	ECHO;
	inComment = FALSE;
	} 

%%

void main(void){
yyin = fopen("../test.c", "r" ); 
yylex();
}

